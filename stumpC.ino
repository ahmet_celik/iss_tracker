void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
    double y = 4.48;
    double x = stumpC( y ); 
    Serial.println(x);
    delay(600);
}

double stumpC( double z )
{
    double c;
    if ( z > 0 )
    {
      c = ( 1 - cos ( sqrt(z) ) ) / z; 
    }  
    else if ( z < 0)
    {    
      c = ( cosh ( sqrt ( -z ) ) - 1 ) / ( -z );
    }
    else  
    {
      c= 1 / 2;
    }   
    return c; 
}
  



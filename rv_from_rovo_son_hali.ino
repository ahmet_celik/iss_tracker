void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
   double rox = 7000;
   double roy = -12124;
   double roz = 0;
   double vox = 2.6679;
   double voy = 4.6210;
   double voz = 0;
   double t = 3600;
//   double f= -18.9048423;
//   double g= 0.0000027;
//   double fDot= -5257788.5000000;
//   double gDot = -378.0968322;
   double Rx = rv_from_rovo_for_Rx(rox, roy, roz, vox, voy, voz, t);
   double Ry = rv_from_rovo_for_Ry(rox, roy, roz, vox, voy, voz, t);
   double Rz = rv_from_rovo_for_Rz(rox, roy, roz, vox, voy, voz, t);
   double Vx = rv_from_rovo_for_Vx(rox, roy, roz, vox, voy, voz, t);
//   double Vy = rv_from_rovo_for_Vy(rox, roy, roz, vox, voy, voz, t);
//   double Vz = rv_from_rovo_for_Vz(rox, roy, roz, vox, voy, voz, t);
   Serial.println(Rx, 7);
   Serial.println(Ry, 7);
   Serial.println(Rz, 7);
   Serial.println(Vx);
//   Serial.println(Vy);
//   Serial.println(Vz);
   delay(600);
}
double rv_from_rovo_for_Rx(double rox, double roy, double roz, double vox, double voy, double voz, double t) 
{
  double mu = 398600;
  double ro  = sqrt((pow(rox,2) + pow(roy,2)  + pow(roz,2)));      //   Magnitude of ro.
  double vo  = sqrt(pow (vox,2)  + pow(voy,2)  + pow(voz,2));       //   Magnitude of vo.
  double vro =  ((rox * vox)    + (roy * voy) + (roz * voz)) / ro; //   Initial radial velocity.
//  double a = (2 / ro)-(pow(vo,2) / mu);                            //   Reciprocal of the semimajor axis.
  double f= -18.9048423;
  double g= 0.0000027;
  double Rx = (f * rox) + (g * vox);                              //    Final position vector in x direction.
  return Rx;
}
double rv_from_rovo_for_Ry(double rox, double roy, double roz, double vox, double voy, double voz, double t)
{
  double mu = 398600;
  double ro  = sqrt((pow(rox,2) + pow(roy,2)  + pow(roz,2)));      //   Magnitude of ro.
  double vo  = sqrt(pow(vox,2)  + pow(voy,2)  + pow(voz,2));       //   Magnitude of vo.
  double vro =  ((rox * vox)    + (roy * voy) + (roz * voz)) / ro; //   Initial radial velocity.
//  double a = (2 / ro)-(pow(vo,2) / mu);                            //   Reciprocal of the semimajor axis.
  double f= -18.9048423;
  double g= 0.0000027;
  double Ry = (f * roy) + (g * voy);                               //   Final position vector in y direction.
  return Ry;
}
double rv_from_rovo_for_Rz(double rox, double roy, double roz, double vox, double voy, double voz, double t)
{
  double mu = 398600;
  double ro  = sqrt((pow(rox,2) + pow(roy,2)  + pow(roz,2)));      //   Magnitude of ro.
  double vo  = sqrt(pow(vox,2)  + pow(voy,2)  + pow(voz,2));       //   Magnitude of vo.
  double vro =  ((rox * vox)    + (roy * voy) + (roz * voz)) / ro; //   Initial radial velocity.
//  double a = (2 / ro)-(pow(vo,2) / mu);                           //    Reciprocal of the semimajor axis.
  double f= -18.9048423;
  double g= 0.0000027;
  double Rz = (f * roz) + (g * voz);                             //     Final position vector in z direction.
  return Rz;
}
double rv_from_rovo_for_Vx(double rox, double roy, double roz, double vox, double voy, double voz, double t)
{
  double mu = 398600;
  double ro  = sqrt((pow(rox,2) + pow(roy,2)  + pow(roz,2)));      //   Magnitude of ro.
  double vo  = sqrt(pow(vox,2)  + pow(voy,2)  + pow(voz,2));       //   Magnitude of vo.
  double vro =  ((rox * vox)    + (roy * voy) + (roz * voz)) / ro; //   Initial radial velocity.
//  double a = (2 / ro)-(pow(vo,2) / mu);                            //   Reciprocal of the semimajor axis.
  double fDot= -5257788.5000000;
  double gDot = -378.0968322;
  double Vx = (fDot * rox) + (gDot * vox);                        //    Final velocity in x direction.
  return Vx;
}
//double rv_from_rovo_for_Vy(double rox, double roy, double roz, double vox, double voy, double voz, double t); 
//{
//  double mu = 398600;
//  double ro  = sqrt((pow(rox,2) + pow(roy,2)  + pow(roz,2)));      //   Magnitude of ro.
//  double vo  = sqrt(pow(vox,2)  + pow(voy,2)  + pow(voz,2));       //   Magnitude of vo.
//  double vro =  ((rox * vox)    + (roy * voy) + (roz * voz)) / ro; //   Initial radial velocity.
//  double a = (2 / ro)-(pow(vo,2) / mu);                            //   Reciprocal of the semimajor axis.
//  double t = 3600;
//  double fDot= -5257788.5000000;
//  double gDot = -378.0968322;
//  double Vy = (fdot * roy) + (gdot * voy);                        //    Final velocity in y direction.
//  return Vy;
//}
//double rv_from_rovo_for_Vz(double rox, double roy, double roz, double vox, double voy, double voz, double t); 
//{
//  double mu = 398600;
//  double ro  = sqrt((pow(rox,2) + pow(roy,2)  + pow(roz,2)));      //   Magnitude of ro.
//  double vo  = sqrt(pow(vox,2)  + pow(voy,2)  + pow(voz,2));       //   Magnitude of vo.
//  double vro =  ((rox * vox)    + (roy * voy) + (roz * voz)) / ro; //   Initial radial velocity.
//  double a = (2 / ro)-(pow(vo,2) / mu);                            //   Reciprocal of the semimajor axis.
//  double t = 3600;
//  double fDot= -5257788.5000000;
//  double gDot = -378.0968322;
//  double Vz = (fdot * roz) + (gdot * voz);                        //    Final velocity in z direction.
//  return Vz;
//}








void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  double a = -19655;
  double t = 3600;
  double x = 253.53;
  double ro = 10000;
  double f = f_func(x, t, ro, a);
  double g = g_func(x, t, ro, a);
  Serial.println(f, 7);
  Serial.println(g, 7);
  delay(600);
}
double f_func(double x,double t,double ro,double a)
{
  double mu = 398600;
  double c = 0.34;
  double f  = (1 - pow(x,2)) / (ro*c);
  return f;
}
double g_func(double x,double t,double ro,double a)
{
  double mu = 398600;
  double s = 0.13;
  double g = (t - 1)/(sqrt(mu) * pow(x,3) * s);
  return g;
}

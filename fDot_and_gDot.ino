void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  double x = 253.53;
  double a = -19655;
  double r = 500;
  double ro =10000;
  double fDot = fDot_func(x, r, ro, a);
  double gDot = gDot_func(x, r, ro, a);
  Serial.println(fDot, 7);
  Serial.println(gDot, 7);
  delay(600);
  

}
double fDot_func(double x,double r,double ro,double a)
{
  double mu = 398600;
  double z = a * pow(x,2);
  double s =  0.13;
  double fdot = ((sqrt(mu) / (r*ro))) * ((z * s) - 1) * x;
  return fdot;
  
}
double gDot_func(double x,double r,double ro,double a)
{
  double mu = 398600;
  double z = a * pow(x,2);
  double c = 0.34;
  double gdot = (1 - pow(x,2)) /( r * c);
  return gdot;
}

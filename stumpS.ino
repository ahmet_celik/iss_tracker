void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
    float y = 4.48;
    float x = stumpS( y );
    Serial.println(x);
    delay(600);
}
float stumpS(float z)
{
  float s ;
  if (z > 0) 
  {
    s = (sqrt(z) - sin(sqrt(z))) / pow((sqrt(z)),3);
  }
  else if (z < 0) 
  {
    s = (sinh(sqrt(-z)) - sqrt(-z)) / pow(sqrt(-z),3);
  }
  else 
  {
    s = 1/6; 
  }
  return s;
}

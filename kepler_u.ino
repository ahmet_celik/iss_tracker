void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  double a = -19655;
  double dt = 3600;
  double ro =10000;
  double vro = 3.0752;
  double x = Kepler_U(dt, ro, vro, a); 
  Serial.println(x,7);
  delay(600);
}  
  double Kepler_U(double dt, double ro, double vro, double a)
{
  double mu=398600;
  double error = 1.e-8;
  double nMax = 1000;
  double x = sqrt(mu) * abs(a) * dt;
  double n =0;
  double ratio = 1;
  
  while (abs(ratio) > error && n <= nMax) 
  {
    n = n + 1;
    double c = 0.34;
    double s = 0.13;
    double F = ((ro * vro) / ((sqrt(mu) * pow(x,2)) * c ))+ (((1 - (a * ro)) * pow(x,3)) * s) + ro * x - sqrt(mu) * dt;
    double dFdx = ((ro * vro) / (sqrt(mu) * x * (1 - ( a * pow(x,2) * s)))) + ((1 - ( a * ro)) * ( pow(x,2) * c )) + ro;
    ratio = F / dFdx;
    x = x - ratio;
  }
  return x;
}


